'use strict';

angular.module('koretouch', ['ngTouch']).
    directive('ngTouchAsClick', [function () {
      return function (scope, element, attrs) {
        element.on('touchend mouseup', function (event) {
          console.log('koretouch event!')
          element.triggerHandler('click', [event]);
        });
      };
    }]);