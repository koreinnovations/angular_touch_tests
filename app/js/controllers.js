'use strict';

/* Controllers */

angular.module('myApp.controllers', ['ngTouch']).
    controller('MyCtrl1', ['$scope', function ($scope) {
        $scope.buttonClick = function () {
            $('#myModal').modal('show');
        };

        $('#test-div').click(function () {
            $scope.buttonClick();
        })

    }])
    .controller('MyCtrl2', [function () {
        var btn = $('#clickme, #clickme2');
        var intervals = [];

        var lightUpIndicator = function (event) {
            $('div[data-event="' + event + '"]').addClass('lit');
            clearTimeout(intervals[event]);
            intervals[event] = setTimeout(function () {
                $('div[data-event="' + event + '"]').removeClass('lit');
            }, 3000);
        };

        btn.on('click', function () {
            lightUpIndicator('click');
        });
        btn.on('tap', function () {
            lightUpIndicator('tap');
        });
        btn.on('touchstart', function () {
            lightUpIndicator('touchstart');
        });
        btn.on('touchend', function () {
            lightUpIndicator('touchend');
        });
        btn.on('touchmove', function () {
            lightUpIndicator('touchmove');
        });
        btn.on('swipe', function () {
            lightUpIndicator('swipe');
        });
        btn.on('mousedown', function () {
            lightUpIndicator('mousedown');
        });
        btn.on('mouseup', function () {
            lightUpIndicator('mouseup');
        });
        btn.on('mousemove', function () {
            lightUpIndicator('mousemove');
        });

    }]);